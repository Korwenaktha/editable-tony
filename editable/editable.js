let container = document.getElementById("editableContainer")
let data = []
let table 

async function getJSON(){
    let url = document.getElementById("editableContainer").getAttribute("data-source")
    try {
        const rep = await fetch(url)
        const json_data = await rep.json()
        data = json_data
        displayHTML()
    } catch (e) {
        console.log(e)
    }
}

function updateData(index, key, value){
data[index][key] = value
}

function displayHTML(){
    table = document.createElement("table")
    container.appendChild(table)
    
    displayBtnInput()
    displayHead()
    displayTable()
}

function displayBtnInput(){
    let input = document.createElement("input")
        input.id = "inputFilter"
        input.className = "rounded-borders"
        input.type = "text"
        input.placeholder = "Filtrer..."
        container.appendChild(input)
        input.addEventListener("input", filter)

    let addBtn = document.createElement("button")
        addBtn.id = "btnAdd"
        addBtn.innerText = "Ajouter"
        container.appendChild(addBtn)
        addBtn.addEventListener("click", add)

    let saveBtn = document.createElement("button")
        saveBtn.id = "btnSave"
        saveBtn.innerText = "Enregistrer"
        container.appendChild(saveBtn)
        saveBtn.addEventListener("click", save)
}

function displayHead(){
    let header = document.createElement("thead")
    let tr = document.createElement("tr")
    tr.className = "rounded-borders"
    let obj = data[0]
    for (const objKey in obj) {
        let th = document.createElement("th")
        th.className = "rounded-borders"
        th.innerText = objKey
        if (th) {
          th.addEventListener("click", sort)
          tr.appendChild(th)
        } else {
          console.error("Unable to create th element for key", objKey)
        }
    }
    header.appendChild(tr)
    table.appendChild(header)
}

function displayTable(){
    let tbody = document.createElement("tbody");
    let index = 0;
    for (const obj of data) {
        let tr = document.createElement("tr");
        tr.className = "rounded-borders"
        tr.classList.add("line");
        for (const key in obj){
            let td = createCell(obj, key, index);
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
        index++;
    }
    if (table) {
        let oldBody = table.getElementsByTagName("tbody")[0];
        if (oldBody) oldBody.remove();
        table.appendChild(tbody);
    }
}

function createCell(obj, key, index){
    let td = document.createElement("td")
    td.className = "rounded-borders"
    td.innerText = obj[key]
    td.contentEditable = "true"
    td.setAttribute("data-key", key)
    td.setAttribute("data-index", index)
    td.addEventListener("input", modifCell)
    return td
}

function filter(e){
    let search = e.target.value
    let lines = document.querySelectorAll(".line")
    for (const line of lines){
        if (line.innerText.includes(search)){
            line.style.display = "table-row"
        } else {
            line.style.display = "none"
        }
    }
}

// function sort(e){
//     let key = e.target.textContent
//     data.sort( (a,b) => a[key].toString().localeCompare(b[key].toString() ) )
//     displayTable()
// }

function sort(e){
    let key = e.target.textContent
    data.sort( (a,b) => {
        let valA = a[key];
        let valB = b[key];
        if(!isNaN(valA) && !isNaN(valB)){
            return valA - valB;
        } else {
            return valA.toString().localeCompare(valB.toString(), 'fr-FR', { sensitivity: 'base' });
        }
    });
    displayTable();
}

function add(e){
    let newObj={}
    for (const key in data[0]){
      newObj[key]=""
    }
    data.push(newObj)
    displayTable()
}

function modifCell(e){
    let val=e.target.innerText
    let key=e.target.getAttribute("data-key")
    let index=e.target.getAttribute("data-index")
    updateData(index, key, val)
}

function save(e){
    let jsonData=JSON.stringify(data)
    fetch("up.php",
      {
        method:"POST",
        body:jsonData
      })
    .then(r=>r.json())
    .then(d=>console.log(d))
}
  
getJSON()